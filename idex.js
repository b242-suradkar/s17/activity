console.log("hello")
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printUserInfo(){
		let name = prompt("What is your name? ");
		console.log("Hello, " + name);
		let age = prompt("How old are you ?");
		console.log("you are " + age + " years old");
		let location = prompt("where do you live? ");
		console.log("you live in " + location);
	}
	printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favMusicArtists(){
		console.log("1. Lata Mangeshkar");
		console.log("2. Shivkumar Sharma");
		console.log("3. Hariprasad Chaurasia");
		console.log("4. T. H. Vinayakram");
		console.log("5. Ravi Shankar");
	}
	favMusicArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovies(){
		console.log("1. RRR");
		console.log("Rotten tomatoes Rating: 95%");
		console.log("2. AVATAR");
		console.log("Rotten tomatoes Rating: 82% ");
		console.log("3. BAAHUBALI: THE BEGINNING");
		console.log("Rotten tomatoes Rating: 90%");
		console.log("4. KANTARA: A LEGEND");
		console.log("Rotten tomatoes Rating: 80%");
		console.log("5. RAM SETU");
		console.log("Rotten tomatoes Rating: 20%");
	}
	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

